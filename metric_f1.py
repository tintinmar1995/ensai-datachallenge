#!/usr/bin/python
# -*- coding: latin-1 -*-

import numpy as np
from sklearn.metrics import cohen_kappa_score, confusion_matrix
from keras.callbacks import Callback
from sklearn.metrics import confusion_matrix, f1_score, precision_score, recall_score
from TP4_ressources.cnn import *
from TP4_ressources.tools import *


class MetricsKappa(Callback):

    def on_train_begin(self, logs={}):
        self.val_kappas = []

    def on_epoch_end(self, epoch, logs={}):
        val_predict = (np.asarray(self.model.predict(self.model.validation_data[0]))).round()
        val_targ = self.model.validation_data[1]
        _val_f1 = f1_score(val_targ, val_predict)
        _val_recall = recall_score(val_targ, val_predict)
        _val_precision = precision_score(val_targ, val_predict)
        self.val_f1s.append(_val_f1)
        self.val_recalls.append(_val_recall)
        self.val_precisions.append(_val_precision)
        print â â val_f1: % f â val_precision: % f â val_recall % fâ % (_val_f1, _val_precision, _val_recall)
        return

