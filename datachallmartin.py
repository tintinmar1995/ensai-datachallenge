# -*- coding: utf-8 -*-
"""
Created on Tue Feb 19 09:43:57 2019

@author: id1157
"""


import pandas as pd
import os
import re
import collections
import numpy as np
from scipy.sparse import coo_matrix
from sklearn.decomposition import TruncatedSVD

# Fonction d'extraction d'informations à partir des url
def getDomain(url):
    i = url.index('//')+2
    try:
        j = url[i:].index('/')
    except:
        j = len(url[i:])

    return url[i:][:j].split(".")[-2]


def getSubDomain(url):
    i = url.index('//')+2
    try:
        j = url[i:].index('/')
    except:
        j = len(url[i:])
    temp = url[i:][:j].split(".")
    if "www" in temp:
        temp.remove("www")
    return temp[:-2]

def getListFolders(url):
    i = url.index('//')+2
    try:
        j = url[i:].index('/')
    except:
        j = len(url[i:])
    temp = url[i:][j:].split("/")
    temp.remove("")
    return temp[:-1]

def getNamePage(url):
    i = url.index('//')+2
    try:
        j = url[i:].index('/')
    except:
        j = len(url[i:])
    temp = url[i:][j:].split("/")
    if "" in temp:
        temp.remove("")
    if len(temp)==0:
        retour=None
    else:
        retour=temp[-1].split('.')[0]
    return retour


def keywords(url):
    retour=[a for a in re.findall("[a-zA-Z]+", url) if len(a)>3]

    if "www" in retour:
        retour.remove("www")

    if "http" in retour:
        retour.remove("http")

    if "https" in retour:
        retour.remove("https")

    if "html" in retour:
        retour.remove("html")

    return retour

def preprocessing(id, date, url):

    #url.split("/")

    domaine = getDomain(url)
    sousdomaine = getSubDomain(url)
    return None


def decoupage():
    print(getDomain("https://www.tutorialspoint.com/python/string_index.htm"))
    print(getSubDomain("https://www.blog.ejc.fr/"))
    print(getListFolders("http://ent4.ensai.fr/uPortal/f/MesFichiers/p/mesfichiers.u161l1n14/max/render.uP?pCp"))
    print(getNamePage("https://www.tutorialspoint.com/python/string_index.htm"))
    print(keywords("https://www.tutorialspoint.com/python/string_index.htm"))

    df = pd.read_csv('train_data.csv', sep=";")
    print(df.columns)

    dom = df.apply(lambda df2: getDomain(df2.PAGE_NAME), axis=1)
    ssdom = df.apply(lambda df2: getSubDomain(df2.PAGE_NAME), axis=1)
    liste = df.apply(lambda df2: getListFolders(df2.PAGE_NAME), axis=1)
    name = df.apply(lambda df2: getNamePage(df2.PAGE_NAME), axis=1)
    kw = df.apply(lambda df2: keywords(df2.PAGE_NAME), axis=1)

    df3 = pd.concat([df, dom, ssdom, liste, name, kw], axis=1, sort=False)
    df3.columns = list(df.columns)+["domaine", "ssdomaine", "dossier", "nom", "keywords"]
    df3.to_csv('train_data_decoup.csv', sep=';')

    return None

def main():
    print(keywords("https://www.tutorialspoint.com/python/str-ing_index.htm"))


def analyse_domaine():
    df = pd.read_csv('train_data_decoup.csv', sep=";")
    #df.columns = ["domaine", "ssdomaine", "dossier", "nom", "keywords"]

    print(len(df.keywords))
    print(len(set(df.keywords)))

    dico = list()
    print(df.keywords)

    for a in df.keywords:
        for b in a[1:-1].split(','):
            temp = b.replace(" ", "")
            temp = temp.replace("'", "")
            dico.append(temp)
    print(len(set(dico)))

    print(collections.Counter(dico).most_common(50))

    return None

def espacevect():
    df = pd.read_csv('train_data_decoup.csv', sep=";")
    dico = list()
    obs = list()

    for a in df.keywords:
        tmp2=list()
        for b in a[1:-1].split(','):
            temp = b.replace(" ", "")
            temp = temp.replace("'", "")
            dico.append(temp)
        obs.append(tmp2)

    all_words = [a[0] in list(collections.Counter(dico).most_common(200))]
    vi=list()
    vj=list()
    vv=list()

    for i, id in enumerate(obs):
        for mot in id:
            vi.append(all_words.index(mot))
            vj.append(i)
            vv.append(1)

    vi = np.array(vi)
    vj = np.array(vj)
    vv = np.array(vv)
    vb = coo_matrix((vv, (vi, vj)), shape=(len(obs), len(dico)))

    clf = TruncatedSVD(100)
    Xpca = clf.fit_transform(vb)


#decoupage()
#analyse_domaine()
espacevect()